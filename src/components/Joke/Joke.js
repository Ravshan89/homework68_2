import React, {Component} from 'react';
import './Joke.css';

class Joke extends Component {
	componentWillMount() {
		console.log('[Joke] Will Mount');
	}
	componentDidMount() {
		console.log('[Joke] Did Mount');
	}
	
	render() {
		console.log('[Joke] render');
		return ( <div className = "Joke" onClick = {this.props.clicked} >
			<h1 > Joke </h1>

			<div className = "Info" >
			<div> {this.props.jokes} </div>
			</div>
			</div>
		);
	}
}

export default Joke;