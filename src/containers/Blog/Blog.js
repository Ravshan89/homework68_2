import React, { Component, Fragment } from 'react'
import './Blog.css'
import Joke from '../../components/Joke/Joke'

class Blog extends Component {
    
    componentDidMount () {
       this.gettingJokes();
    }

    gettingJokes = async () =>{
        const jokesArray =[];
        for(let i=0; i<5; i++){
           const api_url = await fetch('https://api.chucknorris.io/jokes/random');
           const data = await api_url.json();
           jokesArray.push(data)
           console.log(data)
        }
      this.setState({jokes: jokesArray})
   }

    state = {
        jokes: [],
        jokesFormShown: false
    }

   
    toggleJokesForm = () => {
        console.log('[Blog] Toggling form');
        const jokesFormShown = !this.state.jokesFormShown;
        this.setState({ jokesFormShown })
    };

    render() {
        console.log('[Blog] render')
       
        
        return (
            <Fragment>
                <section className="Jokes">
                    {this.state.jokes.map(joke=>(
                        <Joke key={joke.id} jokes={joke.value}/>
                    ))}               
                </section>
                <button className="ToggleButton" onClick={this.gettingJokes}> New Joke</button>
                
                
            </Fragment>
        )
    }
}

export default Blog;